import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-address',
  templateUrl: './address.page.html',
  styleUrls: ['./address.page.scss'],
})
export class AddressPage implements OnInit {
  counter:number;

  constructor(
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) { 

    this.counter=0;
  }

  ngOnInit() {
  }


  add(){
    this.counter= this.counter+1;
  }
  sub(){
    this.counter= this.counter-1;
    if(this.counter<0){
      this.counter=0;
    }
  }

  pagePush(path, data = null) {
    this.router.navigateByUrl(path, { queryParams: data });
    document.body.scrollTop = document.documentElement.scrollTop = 0;
  }

  goBack() {
    this.location.back();
  }

}
